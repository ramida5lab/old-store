import Vue from 'vue'
import VueLazyload from 'vue-lazyload'
import VeeValidate from 'vee-validate'
import VueScrollTo from 'vue-scrollto'
import VModal from 'vue-js-modal/dist/ssr.index'
import Transitions from 'vue2-transitions'
import moment from 'moment'
// require('moment/locale/th')
import fontawesome from '@fortawesome/fontawesome'
import FontAwesomeIcon from '@fortawesome/vue-fontawesome'
import faFacebookSquare from '@fortawesome/fontawesome-free-brands/faFacebookSquare'
import faSpinner from '@fortawesome/fontawesome-pro-solid/faSpinner'
import faSearch from '@fortawesome/fontawesome-pro-solid/faSearch'
import faShoppingCart from '@fortawesome/fontawesome-pro-solid/faShoppingCart'
import faShoppingBag from '@fortawesome/fontawesome-pro-solid/faShoppingBag'
import faTimes from '@fortawesome/fontawesome-pro-light/faTimes'
import faHome from '@fortawesome/fontawesome-pro-solid/faHome'
import faUser from '@fortawesome/fontawesome-pro-solid/faUser'
import faUserCircle from '@fortawesome/fontawesome-pro-solid/faUserCircle'
import faChevronRight from '@fortawesome/fontawesome-pro-solid/faChevronRight'
import faArrowCircleRight from '@fortawesome/fontawesome-pro-solid/faArrowCircleRight'
import faArrowCircleLeft from '@fortawesome/fontawesome-pro-solid/faArrowCircleLeft'
import faClipboardList from '@fortawesome/fontawesome-pro-solid/faClipboardList'
import faCreditCard from '@fortawesome/fontawesome-pro-solid/faCreditCard'
import faUndoAlt from '@fortawesome/fontawesome-pro-solid/faUndoAlt'
import faSignOutAlt from '@fortawesome/fontawesome-pro-solid/faSignOutAlt'
import faLock from '@fortawesome/fontawesome-pro-solid/faLock'
import faCheck from '@fortawesome/fontawesome-pro-solid/faCheck'
import faFrown from '@fortawesome/fontawesome-pro-regular/faFrown'
import faMinus from '@fortawesome/fontawesome-pro-regular/faMinus'
import faPlus from '@fortawesome/fontawesome-pro-regular/faPlus'
import faFacebookF from '@fortawesome/fontawesome-free-brands/faFacebookF'
import faPhone from '@fortawesome/fontawesome-pro-solid/faPhone'
import faInstagram from '@fortawesome/fontawesome-free-brands/faInstagram'
import faEnvelope from '@fortawesome/fontawesome-pro-light/faEnvelope'
import faSearchLight from '@fortawesome/fontawesome-pro-light/faSearch'
import faUserCircleLight from '@fortawesome/fontawesome-pro-light/faUserCircle'
import faKeyboard from '@fortawesome/fontawesome-pro-light/faKeyboard'
import faUsdCircle from '@fortawesome/fontawesome-pro-light/faUsdCircle'
import faBoxFull from '@fortawesome/fontawesome-pro-light/faBoxFull'
import faShare from '@fortawesome/fontawesome-pro-regular/faShareAlt'
import faLine from '@fortawesome/fontawesome-free-brands/faLine'
import faFacebookMessenger from '@fortawesome/fontawesome-free-brands/faFacebookMessenger'
import faFilter from '@fortawesome/fontawesome-pro-light/faFilter'
import faSort from '@fortawesome/fontawesome-pro-solid/faSort'
import faShoppingBagLight from '@fortawesome/fontawesome-pro-light/faShoppingBag'
import faAngleRight from '@fortawesome/fontawesome-pro-regular/faAngleRight'
import faAngleUp from '@fortawesome/fontawesome-pro-regular/faAngleUp'
import faAngleDown from '@fortawesome/fontawesome-pro-regular/faAngleDown'
import faComments from '@fortawesome/fontawesome-pro-light/faComments'
import faMapMarker from '@fortawesome/fontawesome-pro-light/faMapMarkerAlt'
import faTrashAlt from '@fortawesome/fontawesome-pro-light/faTrashAlt'
import faTruck from '@fortawesome/fontawesome-pro-light/faTruck'
import faFileAlt from '@fortawesome/fontawesome-pro-light/faFileAlt'

import * as Auth from '~/services/auth'

Vue.use(VueScrollTo)
Vue.use(VeeValidate)
// VueLazyLoad
Vue.use(VueLazyload, {
  preLoad: 1.3,
  error: '/images/error.png',
  loading: '/images/loading.gif',
  attempt: 1
})
Vue.use(require('vue-moment'), {
  moment
})
Vue.use(VModal, {
  dialog: true
})
Vue.use(Transitions)
fontawesome.config = {
  autoAddCss: false
}
fontawesome.library.add(faFacebookSquare, faFrown, faMinus, faPlus, faSpinner, faSearch, faShoppingCart, faShoppingBag, faTimes, faHome, faUser, faUserCircle, faChevronRight, faArrowCircleLeft,faArrowCircleRight, faClipboardList, faTruck, faCreditCard, faUndoAlt, faSignOutAlt, faLock, faCheck, faFacebookF, faPhone, faInstagram, faEnvelope, faSearchLight, faKeyboard, faUsdCircle, faBoxFull, faShare, faLine, faFacebookMessenger, faFilter, faSort, faShoppingBagLight, faAngleRight, faAngleUp, faAngleDown, faComments, faMapMarker, faTrashAlt, faTruck, faFileAlt,faUserCircleLight)
Vue.component('fa-icon', FontAwesomeIcon)
// Init firebase
Auth.initFirebase()

Vue.filter('currency', val => {
  val = parseFloat(val).toLocaleString('th-TH', {
    minimumFractionDigits: 0,
    maximumFractionDigits: 0
  })
  return `${val} ฿`
})
