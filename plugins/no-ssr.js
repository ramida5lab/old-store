import Vue from 'vue'
import VueFloatLabel from 'vue-float-label'
import VueCard from 'vue-credit-card'
import VueAgile from 'vue-agile'
import VueAffix from 'vue-affix'
import VueScrollReveal from 'vue-scroll-reveal'
import lineClamp from 'vue-line-clamp'
import WebFont from 'webfontloader'

Vue.use(VueScrollReveal, {
  class: 'v-scroll-reveal',
  duration: 500,
  scale: 0.9,
  distance: '50px',
  mobile: false
})
Vue.use(VueFloatLabel)
Vue.use(VueAgile)
Vue.use(VueAffix)
Vue.use(lineClamp)
Vue.component('Card', VueCard)

WebFont.load({
  google: {
    families: ['Montserrat:300,500,700']
  }
})
