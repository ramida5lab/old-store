import Vue from 'vue'

export default ({
  store
}) => {
  if (store.state.site.currency === 'THB') {
    Vue.filter('currency', val => {
      val = parseFloat(val).toLocaleString('th-TH', {
        minimumFractionDigits: 0,
        maximumFractionDigits: 0
      })
      return `${val} ฿`
    })
  }
}
