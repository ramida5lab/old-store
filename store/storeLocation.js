import urls from '~/services/apiUrl'

export const state = () => ({
  locations: []
})

export const actions = {
  async getStoreLocation({
    commit
  }) {
    const locations = await this.$axios.$get(urls.getStoreLocations)
    return commit(`SET_STORE_LOCATIONS`, locations)
  },
}

export const mutations = {
  SET_STORE_LOCATIONS(state, locations) {
    state.locations = locations
  },
}
