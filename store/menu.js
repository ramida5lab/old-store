export const state = () => ({
  isShowing: false,
  isSearchShowing: false,
  isUserMenuShowing: true,
  isUserSticky: false,
  isCartShowing: true,
  headerMenu: [],
  searchMenu: [],
  footerMenu: [{
      title: 'About Us',
      path: '/about-us',
      header: false,
      footer: true, // false
      mobile: true
    },
    {
      title: 'Size Chart',
      path: '/size-chart',
      header: false,
      footer: true, // false
      mobile: false
    },
    {
      title: 'How to order',
      path: '/how-to-order',
      header: false,
      footer: true, // false
      mobile: true
    },
    {
      title: 'Sales & Promotions',
      path: '/sales-promotions',
      header: false,
      footer: true // false
    },
    {
      title: 'Product Review',
      path: '/collections',
      header: false,
      footer: true // false
    },
    {
      title: 'Press & Articles',
      path: '/articles',
      header: false,
      footer: true, // false
      mobile: false
    },
    {
      title: 'Store Locations',
      path: '/store-locations',
      header: false,
      footer: true, // false
      mobile: false
    },
    {
      title: 'Terms & Conditions',
      path: '/terms-conditions',
      header: false,
      footer: true, // false
      mobile: false
    },
    {
      title: 'Return Policy',
      path: '/return-policy',
      header: false,
      footer: true, // false
      mobile: false
    },
    {
      title: 'Contact Us',
      path: '/contact-us',
      header: false,
      footer: true, // false
      mobile: true
    },
    {
      title: 'Account',
      path: '/account',
      header: false,
      footer: false // false
    }
  ],
  headerOrder: [0, 1, 5, 6, 4, 13]
})

export const mutations = {
  SET_CART_SHOWING(state, bool) {
    state.isCartShowing = bool
  },
  SET_HEADER_MENU(state, items) {
    state.headerMenu = items
  },
  SET_MOBILE_MENU(state, bool) {
    state.isShowing = bool
  },
  SET_SEARCH_SHOWING(state, bool) {
    state.isSearchShowing = bool
  },
  SET_SEARCH_MENU(state, items) {
    state.searchMenu = items
  },
  SET_USER_MENU(state, bool) {
    state.isUserMenuShowing = bool
  },
  SET_USER_STICKY(state, bool) {
    state.isUserSticky = bool
  }
}
