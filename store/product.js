import urls from '~/services/apiUrl'

export const state = () => ({
  attributes: {},
  categories: [],
  collections: [],
  sortModes: [{
      slug: 'popular',
      title: 'Popular',
      default: true
    },
    {
      slug: 'price-high-low',
      title: 'Price (High-Low)'
    },
    {
      slug: 'price-low-high',
      title: 'Price (Low-High)'
    },
    {
      slug: 'newest',
      title: 'Newest'
    }
  ],
  salesAndPromotions: []
})

export const actions = {
  async applyCoupon({}, coupon) {
    const res = await this.$axios.$post(urls.coupon, {
      coupon
    })
    return res
  },
  async getCollection({
    commit
  }) {
    const collections = await this.$axios.$get(urls.getCollections)
    return commit(`SET_COLLECTIONS`, collections)
  },
  async getProductsByCollectionSlug({}, {
    collectionSlug
  }) {
    const products = await this.$axios.$get(`${urls.getProducts}?collection=${encodeURI(collectionSlug)}`)
    return products
  },
  async getIndividualCollectionSlug({}, {
    collectionSlug
  }) {
    const collections = await this.$axios.$get(`${urls.getCollections}?slug=${encodeURI(collectionSlug)}`)
    //console.log(collections) 
    return collections
  },
  async getProductsFromSearch({}, {
    q
  }) {
    const products = await this.$axios.$get(`${urls.getProducts}?q=${q}`)
    return products
  },
  async getFeaturedProducts() {
    const products = await this.$axios.$get(`${urls.getProducts}?featured=true`)
    return products
  },
  async getCategories({}, {
    exclude = 0
  }) {
    const cats = await this.$axios.$get(`${urls.getCategories}?exclude=${exclude}`)
    return cats
  },
  async getProductsFromSubcategorySlug({}, {
    subcatSlug
  }) {
    const products = await this.$axios.$get(`${urls.getProducts}?category=${encodeURI(subcatSlug)}`)
    return products
  },
  async getIndividualProductSlug({}, {
    productSlug
  }) {
    const products = await this.$axios.$get(`${urls.getProducts}?slug=${encodeURI(productSlug)}`)
    return products
  },
  async getSalePromotions({
    commit
  }) {
    const salesAndPromotions = await this.$axios.$get(urls.getSalePromotions)
    return commit(`SET_SALE_PROMOTIONS`, salesAndPromotions)
  },
  async getSalePromotionSlug({}, {
    promotion
  }) {
    const salesAndPromotions = await this.$axios.$get(`${urls.getProducts}?promotion=${encodeURI(promotion)}`)
    //console.log(collections) 
    return salesAndPromotions
  }
}

export const mutations = {
  SET_PRODUCT_ATTRIBUTES(state, attributes) {
    state.attributes = attributes
  },
  SET_PRODUCT_CATEGORIES(state, categories) {
    state.categories = categories
  },
  SET_SALE_PROMOTIONS(state, salesAndPromotions) {
    state.salesAndPromotions = salesAndPromotions
  },
  SET_COLLECTIONS(state, collections) {
    state.collections = collections
  },
}
