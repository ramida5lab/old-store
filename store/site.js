import pkg from '~/package'
import urls from '~/services/apiUrl'

export const state = () => ({
  title: `– ${pkg.description}`,
  omisePublicKey: pkg.omisePublicKey,
  termCondition: [],
  privacyPolicy: [],
  returnPolicy: [],
  aboutUs: [],
  usdthb: 33,
  currency: 'thb',
  fabricTechnologies: [],
  countryCodes: {},
  sizeSort: ['fs', 'xs', 's', 'm', 'l', 'xl'],
  socialMedias: [{
      title: 'Phone',
      style: 'fas',
      icon: 'phone',
      path: 'tel:+66924942545',
      text: '+6692 494 2545',
      flip: 'horizontal'
    },
    {
      title: 'Facebook',
      style: 'fab',
      icon: 'facebook-f',
      path: 'http://facebook.com/5labgroup',
      text: '5Lab Store',
      flip: null
    },
    {
      title: 'Instagram',
      style: 'fab',
      icon: 'instagram',
      path: 'https://www.instagram.com/iggu.me',
      text: 'iggu.me',
      flip: null
    },
    {
      title: 'Email',
      style: 'fal',
      icon: 'envelope',
      path: 'mailto:hello@5lab.co',
      text: 'hello@5lab.co',
      flip: null
    }
  ]
})

export const actions = {
  async getSiteInit({
    commit,
    rootState,
  }) {
    const {
      attributes,
      categories,
      collections,
      sale_promotions,
      country_codes,
      usdthb
    } = await this.$axios.$get(urls.getSiteInit)
    commit('SET_USDTHB', parseFloat(usdthb))
    commit('product/SET_PRODUCT_ATTRIBUTES', attributes, {
      root: true
    })
    commit('product/SET_PRODUCT_CATEGORIES', categories, {
      root: true
    })
    commit('product/SET_COLLECTIONS', collections, {
      root: true
    })
    commit('product/SET_SALE_PROMOTIONS', sale_promotions, {
      root: true
    })
    commit('SET_CC', country_codes)
    let headerMenu = [
      ...categories.map((c) => {
        return {
          isActive: false,
          title: `${c.name}`,
          productCategory: true,
          subMenu: c.subcategories.filter(z => z.count).map((s) => {
            return {
              title: s.name,
              path: `/categories/${s.slug}`,
              show: true
            }
          })
        }
      }),
      // {
      //   title: 'how to order',
      //   path: '/how-to-order',
      //   noHeader: true
      // },
      {
        title: 'sale',
        isActive: false,
        subMenu: [
          ...sale_promotions.map((item) => {
            return {
              path: `/sales-promotions/${item.slug}`,
              title: item.title.rendered,
              show: true
            }
          }),
          {
            path: '/sales-promotions',
            title: 'All Sales and Promotions',
            show: true
          }
        ]
      },
      {
        title: 'Review',
        isActive: false,
        subMenu: [
          ...collections.map((item) => {
            return {
              path: `/collections/${item.slug}`,
              title: item.title.rendered,
              show: true
            }
          }),
          {
            path: '/collections',
            title: 'All Product Reviews',
            show: true
          }
        ]
      },
      {
        title: 'account',
        header: rootState.auth.user ? 'GO FOR IT, GIRL.' : 'LET\'S BE FRIENDS.',
        avatar: rootState.auth.user,
        isActive: false,
        subMenu: [{
            title: 'Register',
            path: '/register',
            show: !rootState.auth.user
          },
          {
            title: 'Login',
            path: '/login',
            show: !rootState.auth.user
          },
          {
            title: 'My Account',
            path: '/my-account',
            show: rootState.auth.user
          },
          {
            title: 'My Orders',
            path: '/my-account/orders',
            show: rootState.auth.user
          },
          {
            title: 'Address',
            path: '/my-account/address',
            show: rootState.auth.user
          },
          {
            title: 'Logout',
            path: '/my-account/logout',
            show: rootState.auth.user
          }
        ]
      }
    ]
    commit('menu/SET_HEADER_MENU', headerMenu, {
      root: true
    })
    let searchMenu = [].concat.apply([], headerMenu
      .filter((x) => x.productCategory)
      .map((y) => y.subMenu))
    commit('menu/SET_SEARCH_MENU', searchMenu, {
      root: true
    })
    return
  },
  async getTermCondition({
    commit
  }) {
    const termCondition = await this.$axios.$get(urls.getTermCondition)
    return commit(`SET_TERM_CONDITION`, termCondition)
  },
  async getPrivacyPolicy({
    commit
  }) {
    const privacyPolicy = await this.$axios.$get(urls.getPrivacyPolicy)
    return commit(`SET_PRIVACY_POLICY`, privacyPolicy)
  },
  async getReturnPolicy({
    commit
  }) {
    const returnPolicy = await this.$axios.$get(urls.getReturnPolicy)
    return commit(`SET_RETURN_POLICY`, returnPolicy)
  },
  async sendMail({
    commit
  }, {
    to,
    html
  }) {
    const res = await this.$axios.$post(`${urls.sendMail}`, {
      to,
      html
    })
    return res
  },
  async getAboutUs({
    commit
  }) {
    const aboutUs = await this.$axios.$get(urls.getAboutUs)
    return commit(`SET_ABOUT_US`, aboutUs)
  },
  async getFabricTechnologies({
    commit
  }) {
    const fabricTechnologies = await this.$axios.$get(urls.getFabricTechnologies)
    return commit(`SET_FABRIC_TECHNOLOGIES`, fabricTechnologies)
  },
  async getSlideshow({}, slug) {
    const slideshow = await this.$axios.$get(`${urls.getSlideshowBySlug}?slug=${slug}`)
    if (slideshow) {
      return slideshow[0].acf.slide
    }
    return false
  }
}

export const mutations = {
  SET_USDTHB(state, curr) {
    state.usdthb = curr
  },
  SET_CURRENCY(state, curr) {
    state.currency = curr
  },
  SET_CC(state, cc) {
    state.countryCodes = cc
  },
  SET_TERM_CONDITION(state, termCondition) {
    state.termCondition = termCondition
  },
  SET_PRIVACY_POLICY(state, privacyPolicy) {
    state.privacyPolicy = privacyPolicy
  },
  SET_RETURN_POLICY(state, returnPolicy) {
    state.returnPolicy = returnPolicy
  },
  SET_ABOUT_US(state, aboutUs) {
    state.aboutUs = aboutUs
  },
  SET_FABRIC_TECHNOLOGIES(state, fabricTechnologies) {
    state.fabricTechnologies = fabricTechnologies
  }
}
