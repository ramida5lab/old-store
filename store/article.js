import urls from '~/services/apiUrl'

export const state = () => ({
  articles: []
})

export const actions = {
  async getArticles ({commit}) {
    const articles = await this.$axios.$get(`${urls.getArticles}?per_page=100`)
    return commit(`SET_ARTICLES`,articles)
  },
  async getIndividualArticleSlug({}, {
    articleSlug
  }) {
    const article = await this.$axios.$get(`${urls.getArticles}?slug=${encodeURI(articleSlug)}`)
    return article
  }
}

export const mutations = {
  SET_ARTICLES (state, articles) {
    state.articles = articles
  },
}