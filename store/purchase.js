import urls from '~/services/apiUrl'
import pkg from '~/package.json'

export const state = () => ({
  isCartShowing: false,
  isCartBubbleShowing: false,
  isCartProcessing: false,
  isPurchasing: false,
  isCartLoading: false,
  cart: {
    cart_contents: {}
  },
  gateways: [],
  checkoutStep: 1
})

export const actions = {
  async getCartContent({
    commit
  }) {
    const cart = await this.$axios.$get(`${urls.getCartContent}`)
    return commit('SET_CART_CONTENT', cart)
  },
  async clearCart({
    commit
  }) {
    const empty = await this.$axios.$post(urls.clearCart)
    const cart = await this.$axios.$get(`${urls.getCartContent}`)
    // Clear cookie

    return commit('SET_CART_CONTENT', cart)
  },
  async addToCart({
    commit
  }, {
    id,
    quantity = 1,
    variationId = false,
    data
  }) {
    commit('SET_CART_PROCESSING', true)
    const bool = await this.$axios.$post(`${urls.addToCart}`, {
      id,
      quantity,
      variationId,
      data
    })
    if (bool) {
      const cart = await this.$axios.$get(`${urls.getCartContent}`)
      commit('SET_CART_CONTENT', cart)
    }
    return commit('SET_CART_PROCESSING', false)
  },
  async updateProductQuantity({
    commit
  }, {
    keyId,
    quantity
  }) {
    // commit('SET_CART_PROCESSING', true)
    const remain = await this.$axios.$put(urls.updateProductQuantity, {
      id: keyId,
      quantity
    })
    // commit('SET_CART_PROCESSING', false)
    return commit('SET_CART_CONTENT', remain)
  },
  async setCustomerCountry({
    commit
  }, countryCode) {
    // console.log('setting country code: ', countryCode)
    const {
      cart
    } = await this.$axios.$put(urls.updateCustomerCountry, {
      countryCode
    })
    // console.log(customer)
    // commit('auth/SET_CUSTOMER', customer, {
    //   root: true
    // })
    // commit('SET_CART_PROCESSING', false)
    return commit('SET_CART_CONTENT', cart)
  },
  async removeFromCart({
    commit
  }, keyId) {
    commit('SET_CART_PROCESSING', true)
    const remain = await this.$axios.$put(urls.removeFromCart, {
      id: keyId
    })
    commit('SET_CART_PROCESSING', false)
    return commit('SET_CART_CONTENT', remain)
  },
  async payByCreditCard({
    disatch,
    commit
  }, {
    number,
    name,
    expiry,
    cvc,
    method,
    recurring = false,
    recurringProductId = false
  }) {
    return new Promise(((resolve, reject) => {
      if (Omise) {
        commit('SET_PURCHASING', true)
        Omise.setPublicKey(pkg.omisePublicKey)
        Omise.createToken(
          'card', {
            name,
            number,
            security_code: cvc,
            expiration_month: expiry.mm,
            expiration_year: `20${expiry.yy}`
          },
          (statusCode, res) => {
            if (statusCode === 200) {
              // Success: send back the TOKEN_ID to your server to create a charge.
              // The TOKEN_ID can be found in `response.id`.
              // console.log(res.id)
              this.$axios.$post(urls.chargeCredit, {
                  token: res.id,
                  method,
                  recurring,
                  recurringProductId
                })
                .then((res) => {
                  let formatted = {}
                  Object.entries(res).forEach(([key, value]) => {
                    key = key.replace('\u0000*\u0000', '')
                    formatted[key] = value
                  })
                  resolve(formatted)
                })
                .catch(err => reject(err))
              // Charge at server
            } else {
              // Example Error displaying
              window.alert(res.code + ': ' + res.message)
              commit('SET_PURCHASING', false)
            }
          }
        )
      }
    }))
  },
  async createOrder({
    commit
  }) {
    const res = await this.$axios.$post(urls.order)
    let formatted = {}
    Object.entries(res).forEach(([key, value]) => {
      key = key.replace('\u0000*\u0000', '')
      formatted[key] = value
    })
    return formatted
  },
  async getPaymentGateways({
    commit
  }) {
    const gateways = await this.$axios.$get(urls.payment)
    return gateways
  }
}

export const mutations = {
  SET_STEP(state, step) {
    state.checkoutStep = step
  },
  SET_GATEWAYS(state, gateways) {
    state.gateways = gateways
  },
  CLEAR_CART(state) {
    state.items = []
  },
  SET_CART_LOADING(state, bool) {
    state.isCartLoading = bool
  },
  SET_PURCHASING(state, bool) {
    state.isPurchasing = bool
  },
  SET_CART_PROCESSING(state, bool) {
    state.isCartProcessing = bool
  },
  SET_CART_BUBBLE(state, bool) {
    state.isCartBubbleShowing = bool
  },
  SET_CART_CONTENT(state, content) {
    // console.log(content)
    state.cart = content
  },
  SET_CART_SHOW(state, bool) {
    state.isCartShowing = bool
  },
  SET_PROD_CART_AMT(state, obj = {
    amount: 1,
    id: 1
  }) { // n = 1, -1
    let clonedItems = state.items
    // Find item by obj.id
    const i = clonedItems.findIndex(x => x.id === obj.id)
    clonedItems[i].amount = obj.amount
    state.items = clonedItems
  },
  REMOVE_PROD(state, id) {
    if (id) {
      let clonedItems = state.items.filter(x => x.id !== id)
      state.items = clonedItems
    }
  }
}
