const pkg = require('./package')
const MomentLocalesPlugin = require('moment-locales-webpack-plugin')
const PurgecssPlugin = require('purgecss-webpack-plugin')
const glob = require('glob-all')
const path = require('path')
const axios = require('axios')

module.exports = {
  cache: true,
  mode: 'universal',
  head: {
    title: `${pkg.name} – ${pkg.description}`,
    meta: [{
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no'
      },
      {
        hid: 'description',
        name: 'description',
        content: pkg.description
      },
      {
        hid: 'fbapp',
        property: 'fb:app_id',
        content: pkg.fbAppId
      },
      {
        hid: 'og:site_name',
        property: 'og:site_name',
        content: pkg.url
      },
      {
        hid: 'og:type',
        property: 'og:type',
        content: 'website'
      },
      {
        hid: 'og:locale',
        property: 'og:locale',
        content: 'th_TH'
      },
      {
        hid: 'og:title',
        property: 'og:title',
        content: pkg.description
      },
      {
        hid: 'og:description',
        property: 'og:description',
        content: pkg.description
      },
      {
        hid: 'og:image',
        property: 'og:image',
        content: `${pkg.url}/images/logo-square.png`
      },
      {
        hid: 'theme-color',
        name: 'theme-color',
        content: '#000'
      }
    ],
    // JS
    script: [{
      src: '/js/hotjar.js',
      body: true
    }, {
      src: '/js/offline.min.js',
      body: true
    }],
  },

  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '#000000'
  },
  /*
   ** Global CSS
   */
  css: [{
    src: 'assets/styles/main.scss',
    lang: 'scss'
  }],
  router: {
    scrollBehavior(to, from, savedPosition) {
      if (savedPosition || (to.query && to.query.size)) {
        return savedPosition
      } else {
        let position = {}
        if (to.matched.length < 2) {
          position = {
            x: 0,
            y: 0
          }
        } else if (to.matched.some(r => r.components.default.options.scrollToTop)) {
          position = {
            x: 0,
            y: 0
          }
        }
        if (to.hash) {
          position = {
            selector: to.hash
          }
        }
        return position
      }
    },
    middleware: ['router']
  },
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '~/plugins/ssr.js',
    '~/plugins/currency.js',
    '~/plugins/axios.js',
    {
      src: '~/plugins/no-ssr.js',
      ssr: false
    }
  ],
  render: {
    resourceHints: false
  },
  /*
   ** Nuxt.js modules
   */
  modules: [
    ['@nuxtjs/component-cache', {
      max: 10000,
      maxAge: 60 * 60 * 10 // 10min
    }],
    '@nuxtjs/sitemap', [
      'nuxt-rfg-icon', {
        masterPicture: 'static/images/logo-large.png'
      }
    ],
    '@nuxtjs/toast',
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios', ['@nuxtjs/google-analytics', {
      id: pkg.gaId,
      autoTracking: {
        pageviewOnLoad: false,
        exception: true
      }
    }]
  ],
  sitemap: {
    path: '/sitemap.xml',
    hostname: pkg.url,
    cacheTime: 1000 * 60 * 15,
    gzip: true,
    generate: true,
    exclude: [
      '/set-password',
      '/ping',
      '/my-account',
      '/my-account/**',
      '/checkout',
      '/checkout/**'
    ],
    routes() {
      return axios.get(`https://${pkg.apiUrl}/wp-json/api/v1/products`)
        .then(res => res.data.map(p => '/product/' + p.slug))
    }
  },
  toast: {
    position: 'bottom-right',
  },
  /*
   ** Axios module configuration
   */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    credentials: true,
    debug: false,
    https: true,
    port: 443,
    host: pkg.apiUrl,
    // prefix: '/wp-json/api/v1', moved to apiUrl.js
  },

  /*
   ** Build configuration
   */
  // buildDir: 'nuxt',
  buildDir: 'nuxt',
  build: {
    publicPath: '/',
    plugins: [
      new MomentLocalesPlugin({
        localesToKeep: ['en', 'th'],
      })
    ],
    babel: {
      presets: [
        [
          'env', {
            'targets': {
              'chrome': 52,
              'browsers': ['safari 7', 'ie 11']
            }
          }
        ], 'stage-0', 'stage-1', 'stage-2', 'stage-3'
      ],
      plugins: [
        'transform-runtime',
      ]
    },
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.client) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
      if (!ctx.isDev) {
        config.plugins.push(
          new PurgecssPlugin({
            paths: glob.sync([
              path.join(__dirname, './pages/**/*.vue'),
              path.join(__dirname, './layouts/**/*.vue'),
              path.join(__dirname, './components/**/*.vue')
            ]),
            whitelist: ['html', 'body']
          })
        )
      }
    }
  }
}
