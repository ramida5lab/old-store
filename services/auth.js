import * as firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/storage'

function initFirebase() {
  // Initialize Firebase
  const config = {
    apiKey: "AIzaSyBHiRVBVbPM-5MI9UtFcY2y-ROGxcvazFk",
    authDomain: "fivelabstore.firebaseapp.com",
    databaseURL: "https://fivelabstore.firebaseio.com",
    projectId: "fivelabstore",
    storageBucket: "fivelabstore.appspot.com",
    messagingSenderId: "361418619455"
  }
  if (!firebase.apps.length) {
    return firebase.initializeApp(config)
  }
  return
}

function facebookGetRedirectResult() {
  return firebase
    .auth()
    .getRedirectResult()
}

function facebookSignIn() {
  let provider = new firebase.auth.FacebookAuthProvider()
  // provider.addScope('email')
  // provider.setCustomParameters({
  //   'display': 'popup'
  // })
  return firebase
    .auth()
    .signInWithRedirect(provider)
}

export {
  initFirebase,
  facebookSignIn,
  facebookGetRedirectResult
}
