const prefix = `/wp-json`
const apiv1 = `/api/v1`
const wpv2 = `/wp/v2`

export default {
  // 
  getSiteInit: `${prefix}${apiv1}/site-init`,
  getSlideshowBySlug: `${prefix}${wpv2}/slideshow`,
  // 
  getToken: `${prefix}/jwt-auth/v1/token`, // PUT
  emailRegister: `${prefix}${apiv1}/auth/email`, // POST
  fbLogin: `${prefix}${apiv1}/auth/fb`,
  fbRegister: `${prefix}${apiv1}/auth/fb`,
  logout: `${prefix}${apiv1}/auth/logout`, // POST
  changePassword: `${prefix}${apiv1}/auth/password`, // PUT
  forgotPassword: `${prefix}${apiv1}/auth/forgot_password`, // PUT
  forgotSetPassword: `${prefix}${apiv1}/auth/check_password_key`, // PUT
  /* User */
  updateBasicProfile: `${prefix}${apiv1}/user/me`, // PUT
  customer: `${prefix}${apiv1}/user/customer`,
  getUserMe: `${prefix}${wpv2}/users/me?context=edit`, // PUT
  address: `${prefix}/api/v1/user/address`, // PUT
  // getUserMe: `${prefix}${apiv1}/user`,
  /* Categories with subcats */
  /* Product */
  getProducts: `${prefix}${apiv1}/products`,
  // Cart
  getCartContent: `${prefix}${apiv1}/wc-auth/cart`,
  addToCart: `${prefix}${apiv1}/wc-auth/cart/add`, // POST
  updateCustomerCountry: `${prefix}${apiv1}/wc-auth/cart/country`,
  updateProductQuantity: `${prefix}${apiv1}/wc-auth/cart/update`, // PUT
  clearCart: `${prefix}${apiv1}/wc-auth/cart/clear`, // POST
  removeFromCart: `${prefix}${apiv1}/wc-auth/cart/remove`,
  //Store Locations
  getStoreLocations: `${prefix}${wpv2}/store_location?per_page=100`,
  //Collections
  //getCollections: `${prefix}${wpv2}/collection?per_page=100`,
  getCollections: `${prefix}${wpv2}/collection`,
  //Static Pages
  getTermCondition: `${prefix}${wpv2}/pages/368`,
  getPrivacyPolicy: `${prefix}${wpv2}/pages/408`,
  getReturnPolicy: `${prefix}${wpv2}/pages/406`,
  getAboutUs: `${prefix}${wpv2}/pages/498`,
  getFabricTechnologies: `${prefix}${wpv2}/pages/753`,
  //Articles
  getArticles: `${prefix}${wpv2}/article`,
  // Payment
  payment: `${prefix}${apiv1}/wc-auth/payment`,
  // Countries
  getCountryCodes: `${prefix}${apiv1}/utils/countries`,
  // Order
  order: `${prefix}${apiv1}/wc-auth/order`,
  chargeCredit: `${prefix}${apiv1}/wc-auth/order_credit_charge`,
  // Sales & Promotions
  getSalePromotions: `${prefix}${wpv2}/sale_promotion`,
  // Logout
  logout: `${prefix}/api/v1/auth/logout`,
  // Send mail
  sendMail: `${prefix}/api/v1/utils/send-mail`,
  // Apply coupon
  coupon: `${prefix}/api/v1/coupon`
}
